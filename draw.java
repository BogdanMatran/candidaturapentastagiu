package penta;
import java.awt.*;

public class draw extends Canvas{
	boolean change;
	private static int nr_erori=0;
	private int lungime;
	private char []cuvant;
	private char []rez;
	private boolean [] vizibil;

	draw(char cuv[]){
		
		
		lungime=cuv.length;
		
		cuvant=new char[lungime];
		rez=new char[lungime];
		
		vizibil=new boolean[lungime];
		for(int i=0;i<lungime;i++)
		{
			rez[i]='*';
			cuvant[i]=cuv[i];
			vizibil[i]=false;
		}
		
		
	}
	void Game(char litera){
		change=true;
		for(int i=0;i<lungime;i++){
			if(cuvant[i]==litera){
				vizibil[i]=true;
				rez[i]=cuvant[i];
				change=false;
				repaint();
			}
			
		}
		if(change==true)
		{	nr_erori++;
			repaint();
		}
	}
	
	
	boolean Get_vizibil(){
		for(int i=0;i<lungime;i++){
			if(vizibil[i]==false)
				return false;
		}
		return true;
	}
	int Get_erori(){
		return nr_erori;
	}
	public void paint(Graphics g){
		g.setColor(Color.BLACK);
		g.fillRect(40, 260, 80, 30);
		g.fillRect(60, 40, 40, 220);
		g.fillRect(100,40,130,40);
		g.fillRect(190, 80, 16, 40);
		if(nr_erori==1)
		g.drawOval(172, 120, 51, 51);
		g.setFont(new Font("Courier",Font.PLAIN,15));
		String s1=new String(rez);
		g.drawString("Cuvant "+s1, 70, 400);
		g.drawString("Incercari gresite "+nr_erori, 70, 430);
		if(nr_erori==2){
			g.drawOval(172, 120, 51, 51);
			g.fillRect(195, 170, 3, 70);
		}
		if(nr_erori==3){
			g.drawOval(172, 120, 51, 51);
			g.fillRect(195, 170, 3, 70);
			g.drawLine(195, 170, 155, 190);
		}
		if(nr_erori==4){
			g.drawOval(172, 120, 51, 51);
			g.fillRect(195, 170, 3, 70);
			g.drawLine(195, 170, 155, 190);
			g.drawLine(198, 170, 235, 190);
		}
		if(nr_erori==5){
			g.drawOval(172, 120, 51, 51);
			g.fillRect(195, 170, 3, 70);
			g.drawLine(195, 170, 155, 190);
			g.drawLine(198, 170, 235, 190);
			g.drawLine(195, 240, 155, 270);
		}
		if(nr_erori==6){
			g.drawOval(172, 120, 51, 51);
			g.fillRect(195, 170, 3, 70);
			g.drawLine(195, 170, 155, 190);
			g.drawLine(198, 170, 235, 190);
			g.drawLine(195, 240, 155, 270);
			g.drawLine(198, 240, 235, 270);
		}
		
		
		
		
	}

}
